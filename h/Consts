/*
 * Copyright (c) 2000, Rik Griffin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/* TreeView.Consts.h		*/
/* Rik Griffin Oct 2003		*/

/* define or not to turn debugging on or off	*/
#ifndef Debug_On
 #define Debug_On		0
#endif

/* level of debugging info sent to stderr:	*/
/* from 0 (none) to 3 (verbose)			*/
#define Debug_Level 		1

#define Program_Error                0x1b000000
#define TreeView_ErrorBase           (Program_Error | 0x821B00)
#define TreeView_AllocFailed         (TreeView_ErrorBase+0x01)
#define TreeView_BadNodeID           (TreeView_ErrorBase+0x14)
#define TreeView_BadNodeHandle       (TreeView_ErrorBase+0x15)
#define TreeView_BadNodeGuard        (TreeView_ErrorBase+0x16)
#define TreeView_NoSelection         (TreeView_ErrorBase+0x17)
#define TreeView_EndOfSelection      (TreeView_ErrorBase+0x18)
#define TreeView_BadTemplate         (TreeView_ErrorBase+0x19)
#define TreeView_FailedToCreate      (TreeView_ErrorBase+0x1a)
#define TreeView_NotMenu             (TreeView_ErrorBase+0x1b)
#define TreeView_CurrentNI           (TreeView_ErrorBase+0x20)
#define TreeView_PreviousNI          (TreeView_ErrorBase+0x21)
#define TreeView_NextNI              (TreeView_ErrorBase+0x22)
#define TreeView_ParentNI            (TreeView_ErrorBase+0x23)
#define TreeView_ChildNI             (TreeView_ErrorBase+0x24)

#define X_GAP		8	// gap between sprite and text
#define Y_GAP		8	// as above, if text is below sprite

#define SORT_LIST_CHUNK 16

#ifdef MemCheck_MEMCHECK
#undef _mem_allocate
#undef _mem_free
#undef _mem_extend

#define _mem_allocate(x) mem_allocate_wrapper(x,__FILE__,__LINE__)
#define _mem_free(x) mem_free_wrapper(x,__FILE__,__LINE__)
#define _mem_extend(x,y) mem_extend_wrapper(x,y,__FILE__,__LINE__)

extern void *mem_allocate_wrapper(int x, char *file, int line);
extern void mem_free_wrapper(void *x, char *file, int line);
extern void *mem_extend_wrapper(void *x, int y, char *file, int line);

#else
#define _mem_allocate(x) mem_allocate(x)
#define _mem_free(x) mem_free(x)
#define _mem_extend(x,y) mem_extend(x,y)
#endif


typedef struct {
  int size;
  int num_sprites;
  int sprite_offset;
  int free_offset;
} SpriteArea;


typedef struct {
  int next_sprite;
  char name[12];
  int width;
  int height;
  int first_bit_used;
  int last_bit_used;
  int image_offset;
  int mask_offset;
  int mode;
} SpriteCtrlBlock;
